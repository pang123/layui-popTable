## layui-popTable：基于layui开发的自定义弹窗表格组件
### 1. 功能说明
- 指定的按钮的单击事件弹窗 或者直接绑定到input文本框的单击事件中
- 弹窗的表格支持实时检索,需要和后台配合使用(后台查询带keywords参数)

### 2. 使用案例截图

![avatar](popTable.gif)

### 3. 案例代码

```
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>测试弹窗</title>
  <script src="layui/layui.js" charset="utf-8"></script>
  <link rel="stylesheet" href="layui/css/layui.css" media="all">
  <link rel="stylesheet" href="layui/css/layform_sm.css" media="all">
</head>

<body>
  <div class="layui-fluid">
    <div class="layui-card">
      <div class="layui-card-header">基础数据 / 商品列表</div>
      <div class="layui-form layui-card-body layuiadmin-card-header-auto">
        <div class="layui-form-item">

          <div class="layui-inline">
            <label class="layui-form-label-sm">商品编码</label>
            <div class="layui-input-inline-sm">
              <input type="text" id="ptCode" name="ptCode" placeholder="请输入" autocomplete="off" class="layui-input-sm">
            </div>
          </div>

          <div class="layui-inline">
            <label class="layui-form-label-sm">规格型号</label>
            <div class="layui-input-inline-sm">
              <input type="text" name="ptRuleModel" placeholder="请输入" autocomplete="off" class="layui-input-sm">
            </div>
          </div>

          <div class="layui-inline">
            <button class="layui-btn layui-btn-sm layuiadmin-btn-list" lay-submit lay-filter="lay-search">
              <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
            </button>
          </div>
        </div>
      </div>


    </div>
  </div>

  <script>
    layui.config({ base: './lib/' });
    layui.use(['layer', 'popTable'], function () {
      var $ = layui.$, popTable = layui.popTable;


      //添加文本框的弹窗事件
      $('<button id="popSearchBtn" class="layui-btn layui-btn-sm" style="width: 27px;height: 27px;padding-left:5px;margin-left:-4px;margin-top: -2px;"><i class="layui-icon layui-icon-template-1" style="padding: 0px"></i></button>')
        .insertAfter('#ptCode');
      $('#popSearchBtn').click(function () {

        popTable.open({
          inputId: 'ptCode',    //选中的值填入父层的字段id
          title: '商品搜索列表',  //弹窗标题
          url: 'data/data.js', //数据url
          field: 'username',       //选择哪个字段的值
          keyName: 'words',  //向后台服务器发送搜索的字段名，默认是keywords
          offset: ['150px;', '350px;'],
          request: {pageName: 'pageNo',limitName: 'pageSize'},
          cols: [[ //表头
            { field: 'id', title: 'ID', width: 80, sort: true, fixed: 'left' }
            , { field: 'username', title: '用户名', width: 80 }
            , { field: 'sex', title: '性别', width: 80, sort: true }
            , { field: 'city', title: '城市', width: 80 }
            , { field: 'sign', title: '签名', width: 177 }
            , { field: 'experience', title: '积分', width: 80, sort: true }
            , { field: 'score', title: '评分', width: 80, sort: true }
            , { field: 'classify', title: '职业', width: 80 }
            , { field: 'wealth', title: '财富', width: 135, sort: true }
          ]]

        });

      });
    });

  </script>
</body>

</html>

```

### 4. 自定义参数说明

|参数列表 | 参数说明 | 是否必填 | 默认值 |
|- | - | - | - |
| inputId | 父层的input文本框字段id, 弹窗选中的值填充在这个文本框  |  是 | | 
| title | 弹出窗口的标题行 | 是 |  |
| url | 表格数据源连接 | 是 |  |
| field | 选中行时，取的行的值的字段名 | 是 |  |
| keyName | 向后台服务器发送搜索的字段名，默认是keywords | 否 | keywords |
| cols | 表格列字段说明, 同layui的table组件参数 | 是 |  |
| offset | 弹窗的定位，同layui的table组件参数 | 否 | ['150px;','350px;'] |

#### 除了上面列出来的以外，其他layui的table组件的参数都支持
[layui官方table参数说明查看](https://www.layui.com/doc/modules/table.html#options)

#### 有喜欢的帮忙点个star











