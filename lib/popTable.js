

layui.define(['layer','table','form'],function(exports){
  "use strict";
  var $ = layui.$
    ,table = layui.table
    ,form = layui.form;


  var randomId = Math.floor(Math.random()*100000 + 1);
  var formId = 'form_' + randomId;
  var elemId = 'poptpl_' + randomId;
  var keywordsId = 'keywords_' + randomId;

  //设置默认配置
  var defaultOption = {
    filterId: formId
    ,inputId: undefined
    ,title: undefined
    ,tableElem: elemId
    ,url:undefined
    ,cols:[]
    ,page: true
    ,limit: 10
    ,limits: [10, 15, 20, 25, 30]
    ,size: 'sm'
    // ,area: '40%'
    ,offset: ['150px;','350px;']
    ,fixed: false
    ,field: undefined
    ,keywordsId: keywordsId
    ,keyName: 'keywords'
    ,success: undefined
    ,request: undefined

  };

  //增加属性
  var popTable = {
    open:function(options){

      var options = $.extend({},defaultOption,options);
      var popContent =[
        '<div class="layui-form layui-row layui-card-body" lay-filter="'+options.filterId+'">'
        ,'<div class="demoTable">'
        ,'<div class="layui-block">'
        ,'<input class="layui-input" name="'+options.keywordsId+'" id="'+options.keywordsId+'" placeholder="请输入关键字" autocomplete="off">'
        ,'</div>'
        ,'</div>'
        ,'<table id="'+options.tableElem+'" lay-filter="'+options.tableElem+'"></table>'
        ,'<script type="text/html" id="lay-toolbar">'
        ,'<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="selected"><i class="layui-icon layui-icon-ok-circle"></i>选择</a>'
        ,'</script>'
        ,'</div>'
      ].join('');

      var index = layer.open({
        type: 1
        // ,skin: 'layui-layer-molv'
        ,title: options.title
        ,content:popContent //不允许出现滚动条
        ,area: options.area
        ,offset: options.offset
        ,fixed: options.fixed
        // ,success: function (layero, index) {
        //     layer.iframeAuto(index);
        //  }
      });

      table.render({
        elem: '#'+options.tableElem
        ,url: options.url
        ,cols: options.cols
        ,page: options.page
        ,limit: options.limit
        ,limits: options.limits
        ,size: options.size
        ,field: options.field
        ,request: options.request
      });

      //监听行工具条 - 选择
      table.on('tool('+options.tableElem+')', function(obj){
        var selected = obj.data;
        var layEvent = obj.event;
        if(layEvent === 'selected'){
          $('#'+options.inputId).val(selected[options.field]);
          layer.close(index);
        }
      });

      table.on('rowDouble('+options.tableElem+')', function(obj){
        var selected = obj.data;
        $('#'+options.inputId).val(selected[options.field]);
        layer.close(index);
      });
      form.render(null, options.filterId);

      //搜索输入框的值改变时，触发实时查询
      $("#"+options.keywordsId).on("input",function(e){
        //获取input输入的值
        var keywords = e.delegateTarget.value;
        var keyfields = options.keyName;
        table.reload(options.tableElem, {where: {[keyfields]: keywords},page:{curr:1}});
      });
    }


  };
  exports('popTable', popTable);

});